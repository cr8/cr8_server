const serverless = require('serverless-http')
const express = require('express');
const bodyParser = require('body-parser');
const app = new express();

app.use(require('cors')());
app.use(express.json());
app.use(bodyParser.json({ strict: false }));

require('./routes/admin/admin')(app)


app.get('/', async(req, res) => {
    res.send('index-dev');
})

// app.listen(3000, () => {
//     console.log('http://localhost:3000/');
// })

 module.exports.handler = serverless(app,{
    binary: ['multipart/form-data']
  });