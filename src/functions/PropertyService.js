const util = require('./Utils');
const dynamoose = require('../utils/db')()
const Property = require('../models/Property')(dynamoose)
const Company = require('../models/Company')(dynamoose)
const UserService = require('./UserService')

/**
 *
 * @param event
 * @returns {Promise<{headers: {"Access-Control-Allow-Origin": string, "Access-Control-Allow-Credentials": boolean, "Content-Type": string}, body: string, statusCode: number}>}
 */
module.exports.loadPublicProperties = async (event) => {
    let companyId = event.headers['companyid'];
    const projectAtts = ["companyId","id","parentId","originId","published","status","projectName","location","address","updatedAt","images","fromAgent","category","lng","lat","commission","gst","public","availProperties"];
    let items = await Property.query('companyId').eq(companyId).attributes(projectAtts).exec();
    items = items.filter(p=>p.status == "Published");

    for (let property of items) {
        if (property.originId) {
            let parts = property.originId.split('|');
            let origin = await Property.get({'companyId': parts[0],'id': parts[1]});
            if (origin) {
                Object.keys(origin).forEach(key => {
                    if (!property[key] && (typeof property[key]) != "boolean") {
                        property[key] = origin[key];
                    }
                });
            }
        }
    }
    return util.opt(null, items);
}

/**
 *
 * @param event
 * @returns {Promise<{headers: {"Access-Control-Allow-Origin": string, "Access-Control-Allow-Credentials": boolean, "Content-Type": string}, body: string, statusCode: number}>}
 */
module.exports.loadProperties = async (event) => {
    let companyId = event.headers['companyid'];
    const projectAtts = ["companyId","id","originId","published","status","projectName","location","address","updatedAt","images","fromAgent"];
    let items = await Property.query('companyId').eq(companyId).attributes(projectAtts).exec();

    for (let property of items) {
        if (property.originId) {
            let parts = property.originId.split('|');
            let origin = await Property.get({'companyId': parts[0],'id': parts[1]});
            if (origin) {
                Object.keys(origin).forEach(key => {
                    if (!property[key] && (typeof property[key]) != "boolean") {
                        property[key] = origin[key];
                    }
                });
            }
        }
    }
    return util.opt(null, items);
}

/**
 *
 * @param event
 * @returns {Promise<{headers: {"Access-Control-Allow-Origin": string, "Access-Control-Allow-Credentials": boolean, "Content-Type": string}, body: string, statusCode: number}>}
 */
module.exports.loadPropertyById = async (event) => {
    let companyId = event.headers['companyid'];
    let property = await Property.get({'companyId':companyId,'id': event.pathParameters.id});
    if (property.originId) {
        let parts = property.originId.split('|');
        let origin = await Property.get({'companyId': parts[0],'id': parts[1]});
        if (origin) {
            Object.keys(origin).forEach(key => {
                if (!property[key] && (typeof property[key]) != "boolean") {
                    property[key] = origin[key];
                }
            });
            //property['originStatus'] =  origin['published'];
        }
    }

    return util.opt(null, property);
}

/**
 * Delete the property by company & property id
 *
 * @param event
 * @returns {Promise<{statusCode: number}>}
 */
module.exports.deleteProperty = async (event) => {
    let companyId = event.headers['companyid'];
    await Property.delete({ 'companyId':companyId, 'id': event.pathParameters.id});
    return {statusCode: 202};
}

/**
 *
 * @param event
 * @returns {Promise<{headers: {"Access-Control-Allow-Origin": string, "Access-Control-Allow-Credentials": boolean, "Content-Type": string}, body: string, statusCode: number}>}
 */
module.exports.loadPropertyByShareId = async (event) => {
    const projectAtts = ["attachments","companyId","videos","sales","priceList","images","lat","lng","address","estCompletion","schools","projectName","category","desc"];
    let project = await Property.get({'companyId':event.pathParameters.companyId,'id': event.pathParameters.id}, {"attributes": projectAtts});
    if(project) {
        const company = await Company.get(project.companyId,{"attributes":["name","logo","phone"]});
        const sales = await UserService.getUsersByIds(project.sales);
        project.sales = sales;
        project.company = company;
    }

    return util.opt(null, project);
}