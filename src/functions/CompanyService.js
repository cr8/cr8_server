const dynamoose = require('../utils/db')()
const Company = require('../models/Company')(dynamoose)
const util = require('./Utils');

const to = promise => promise.then(data => [null, data]).catch(error => [(error)]);

module.exports.get = async (event) => {
    const [error, item] = await to(Company.get({id: event.pathParameters.id}));
    return util.opt(error, item);
}