const AWS = require('aws-sdk');
const util = require('./Utils');
const s3 = new AWS.S3({
  region: 'ap-southeast-2'
});

const params = {
  Bucket: process.env.BUCKET,
  Expires: 300,
};

module.exports.getS3PreSignedURL = async (event) => {
  const group = util.tokenClaims(event, "cognito:groups");
  const userId = util.tokenClaims(event, "sub");
  if(event.queryStringParameters.type == "staff") {
    let timestamp = Date.now();
    params['ContentType'] = 'image/png';
    params['Key'] = `${group}/staff/${userId}_${timestamp}.png`;
  } else {
    params['ContentType'] = 'multipart/form-data';
    params['Key'] = event.headers['companyid'] + "/property/" + event.headers['id'] + '/' + event.headers['filename'];
  }
  const signedUrl = s3.getSignedUrl('putObject', params);

  return util.opt(null, {"signedUrl": signedUrl,"key":params.Key});
}