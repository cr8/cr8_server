const dynamoose = require('../utils/db')()
const Message = require('../models/Message')(dynamoose)
const util = require('./Utils');

/**
 *
 * @param event
 * @returns {Promise<*|{headers, body, statusCode}>}
 */
module.exports.top3Messages = async (event) => {
  const email = event.queryStringParameters.email;
  const msgList = await Message.query('email').eq(email).filter('viewed').eq(false).descending().limit(3).exec();

  return util.opt(null, msgList);
}

/**
 * change the viewed status to true
 * @param event
 * @returns {Promise<*|{headers, body, statusCode}>}
 */
module.exports.viewMessage = async (event) => {
  let msg = await Message.queryOne('id').eq(event.pathParameters.id).exec();
  msg.viewed = true;
  await Message.batchPut([msg]);

  return util.opt(null, msg);
}

/**
 * Delete the message by id
 * @param event
 * @returns {Promise<*|{headers, body, statusCode}>}
 */
module.exports.deleteMessage = async (event) => {
  let msg = await Message.delete({id: event.pathParameters.id});

  return util.opt(null, msg);
}


/**
 * load the messages by page
 *
 * @param event
 * @returns {Promise<*|{headers, body, statusCode}>}
 */
module.exports.loadMessages = async (event) => {
  const email = event.queryStringParameters.email;
  const msgList = await Message.query('email').eq(email).descending().exec();

  return util.opt(null, msgList);
}