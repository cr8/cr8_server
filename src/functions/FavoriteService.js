const dynamoose = require('../utils/db')()
const Favorite = require('../models/Favorite')(dynamoose)
const Property = require('../models/Property')(dynamoose)
const util = require('./Utils');

/**
 * Save the project as favorite
 *
 * @param event
 * @returns {Promise<*|{headers, body, statusCode}>}
 */
module.exports.save = async (event) => {
  const {userId, projectId} = JSON.parse(event.body);
  await Favorite.update({'userId':userId,'projectId': projectId});

  return util.opt(null, "Favorite Saved");
}

/**
 * Remove the project from favorite
 *
 * @param event
 * @returns {Promise<*|{headers, body, statusCode}>}
 */
module.exports.delete = async (event) => {
  const {userId, projectId} = JSON.parse(event.body);
  await Favorite.delete({'userId':userId,'projectId': projectId});

  return util.opt(null, "Favorite Removed");
}

/**
 * get the project keys
 *
 * @param event
 * @returns {Promise<void>}
 */
module.exports.listFavKeys = async (event) => {
  let userId = event.pathParameters.id;
  let projectIds = await Favorite.query('userId').eq(userId).attributes(["projectId"]).exec();
  return util.opt(null, projectIds);
}

/**
 * list the projects by userid
 *
 * @param event
 * @returns {Promise<void>}
 */
module.exports.listFavorites = async (event) => {
  let userId = event.pathParameters.id;
  const projectAtts = ["companyId","id","parentId","originId","published","status","projectName","location","address","updatedAt","images","fromAgent","category","lng","lat","commission","gst","public","availProperties"];
  let projectIds = await Favorite.query('userId').eq(userId).attributes(["projectId"]).exec();
  let IdKeys = projectIds.map(p=>{
    const v = p.projectId.split("|");
    return {"companyId":v[0], "id":v[1]};
  })
  const projects = await Property.batchGet(IdKeys, {"attributes": projectAtts});

  return util.opt(null, projects);
}