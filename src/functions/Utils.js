module.exports = {

  opt: function _opt(error, result) {
    let response = {
      statusCode: 200,
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*', // Required for CORS support to work
        'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
      },
      body: JSON.stringify(result),
    };
    if (error) {
      response = {
        statusCode: error.statusCode,
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*', // Required for CORS support to work
          'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
        },
        body: error.message,
      };
    }

    return response;
  },

  tokenClaims: function _(event, claim) {
    if(process.env.IS_OFFLINE === 'true') {
      let value = event.requestContext.authorizer.claims[claim]
      if(typeof value === "string") return value;
      return value[0];
    } else {
      let value = event.requestContext.authorizer.jwt.claims[claim];
      return value.includes('[') ? value.slice( 1, -1): value;
    }
  }

}
