const AWS = require('aws-sdk');
const util = require('./Utils');
const _ = require('lodash');
const shortid = require('shortid');

/**
 *
 * @param event
 * @returns {Promise<{headers: {"Access-Control-Allow-Origin": string, "Access-Control-Allow-Credentials": boolean, "Content-Type": string}, body: *, statusCode: *}>}
 */
module.exports.list = async (event) => {
  const group = util.tokenClaims(event, "cognito:groups");
  if (group) {
    let params = {
      GroupName: 'pending',
      UserPoolId: `${process.env.USER_POOL_ID}`,
    };
    const provider = new AWS.CognitoIdentityServiceProvider();
    const cognitoUsers = await provider.listUsersInGroup(params).promise();

    let users = _.map(cognitoUsers.Users, obj => {
      if (obj.UserStatus != "UNCONFIRMED") {
        let user = new Object;
        user.status = obj.UserStatus;
        user.enable = obj.Enabled;
        _.forEach(obj.Attributes, attr => user[attr.Name] = attr.Value);
        return user;
      }
    })
    users = users.filter(u => u != null);
    return util.opt(null, users);
  }
}


/**
 * approve the account, generate the company id and user group for this account
 *
 * @param event
 * @returns {Promise<{headers: {"Access-Control-Allow-Origin": string, "Access-Control-Allow-Credentials": boolean, "Content-Type": string}, body: *, statusCode: *}>}
 */
module.exports.approveAccount = async (event) => {
  const id = event.pathParameters.id;
  const companyId = shortid.generate();

  const params = {
    GroupName: companyId,
    UserPoolId: `${process.env.USER_POOL_ID}`
  };
  const provider = new AWS.CognitoIdentityServiceProvider();
  await provider.createGroup(params).promise();

  params.Username = id;
  await provider.adminAddUserToGroup(params).promise();

  params.GroupName = 'pending';
  await provider.adminRemoveUserFromGroup(params).promise();

  return util.opt(null, "Approve the account");
}




