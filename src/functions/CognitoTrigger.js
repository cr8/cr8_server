const AWS = require('aws-sdk');

/**
 * move the new user(account) excluding the email invited uses to the pending group, waiting for email confirmation
 *
 * @param event
 * @returns {Promise<{statusCode: number}>}
 */
module.exports.customMessageTrigger = (event, context, callback) => {
  if (event.triggerSource == "CustomMessage_SignUp") {
    let params = {
      GroupName: 'pending',
      UserPoolId: `${process.env.USER_POOL_ID}`,
      Username: event.userName
    };

    const provider = new AWS.CognitoIdentityServiceProvider();
    provider.adminAddUserToGroup(params, function (err, data) {
      if (err) {
        callback(err) // an error occurred
      }
      callback(null, event); // successful response
    });
  } else {
    callback(null, event);
  }

};


/**
 * After user confirmed the email, send the reminder to weproperty
 *
 * @param event
 * @param context
 * @param callback
 */
module.exports.postConfirmationTrigger = (event, context, callback) => {
  if (event.request.userAttributes.email) {
    let sub = event.request.userAttributes.email + " Just Joined";
    let body = "New User " + event.request.userAttributes.email + " Just confirmed email";
    sendEmail(event.request.userAttributes.email, sub, body, function (status) {
      // Return to Amazon Cognito
      callback(null, event);
    });
  } else {
    callback(null, event);
  }
}


function sendEmail(to, sub, body, completedCallback) {
  var eParams = {
    Destination: {
      ToAddresses: ['sales@weproperty.com.au']
    },
    Message: {
      Body: {
        Text: {
          Data: body
        }
      },
      Subject: {
        Data: sub
      }
    },

    Source: "info@weproperty.com.au"
  };

  let ses = new AWS.SES({region: "ap-southeast-2"});
  ses.sendEmail(eParams, function (err, data) {
    if (err) {
      console.log(err);
    } else {
      console.log("===EMAIL SENT===");
    }
    completedCallback('Email sent');
  });
};