const AWS = require('aws-sdk');
const _ = require('lodash');
const dynamoose = require('../utils/db')()
const Property = require('../models/Property')(dynamoose)
const util = require('./Utils');

/**
 *
 * @param event
 * @returns {Promise<{headers: {"Access-Control-Allow-Origin": string, "Access-Control-Allow-Credentials": boolean, "Content-Type": string}, body: string, statusCode: number}>}
 */
module.exports.updateAgentSharing = async (event) => {
  for (let record of event.Records) {
    if (record.eventName === "INSERT") {
      let newImage = AWS.DynamoDB.Converter.unmarshall(record.dynamodb.NewImage);
      if (newImage.agents) {
        newImage.agents = JSON.parse(newImage.agents);
        let items = _.map(newImage.agents, agent => {
          let tmp = _.assign({}, newImage);
          tmp.id = null;
          tmp.parentId = newImage.id;
          tmp.published = false;
          tmp.status = "Draft";
          tmp.agents = null;
          tmp.sales = null;
          tmp.companyId = agent;
          tmp.originId = `${newImage.companyId}|${newImage.id}`;
          tmp.fromAgent = newImage.fromAgent;
          tmp.fromAgentId = newImage.companyId;
          tmp.fromAgentCOMSN = newImage.commission;
          tmp.commission = "N/A";
          return tmp;
        });
        await Property.batchPut(items);
      }
    } else if (record.eventName === 'MODIFY') {
      let oldImage = AWS.DynamoDB.Converter.unmarshall(record.dynamodb.OldImage);
      let newImage = AWS.DynamoDB.Converter.unmarshall(record.dynamodb.NewImage);

    } else if (record.eventName === 'REMOVE') {
      let oldImage = AWS.DynamoDB.Converter.unmarshall(record.dynamodb.OldImage);
      if (oldImage.agents) {
        oldImage.agents = JSON.parse(oldImage.agents);
        for (let agent of oldImage.agents) {
          await Property.update({'companyId': agent,'parentId': oldImage.id},{'published': false, 'status': 'Obsolete'});
        }
      }
    }
  }

  return util.opt(null, "unmarshall the image");
}


module.exports.updateChannelSharing = async (event) => {

  return util.opt(null, "Updated the sharing");
}


