const util = require('./Utils');
const rq = require('request-promise');
const sha1 = require('sha1')

const to = promise => promise.then(data => [null, data]).catch(error => [(error)]);

module.exports.getWXSign = async (event) => {
  let nonce_str = '123456';
  let appId = 'wxb950ecba7ad92987';
  let secret = 'e7317d9f4e529712656652c46b623e36';
  let timestamp = new Date().getTime();
  let url = event.queryStringParameters.url;
  const TOKEN_REQ = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=' + appId + '&secret=' + secret;

  const [err1, response] = await to(rq(TOKEN_REQ));
  const tokenMap = JSON.parse(response);
  const [err2, result] = await to(rq('https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=' + tokenMap.access_token + '&type=jsapi'));
  const ticketMap = JSON.parse(result);
  obj = {
    appId: appId,
    noncestr: nonce_str,
    timestamp: timestamp,
    url: url,
    jsapi_ticket: ticketMap.ticket,
    signature: sha1('jsapi_ticket=' + ticketMap.ticket + '&noncestr=' + nonce_str + '&timestamp=' + timestamp + '&url=' + url)
  }
  console.log('return opt ...:' + JSON.stringify(obj));
  return util.opt(null, obj);

}