const AWS = require('aws-sdk');
const util = require('./Utils');
const _ = require('lodash');

const provider = new AWS.CognitoIdentityServiceProvider();
const to = promise => promise.then(data => [null, data]).catch(error => [(error)]);

/**
 *
 * @param event
 * @returns {Promise<{headers: {"Access-Control-Allow-Origin": string, "Access-Control-Allow-Credentials": boolean, "Content-Type": string}, body: *, statusCode: *}>}
 */
module.exports.list = async (event) => {
  if (!isAdminUser(event))
    return util.opt({statusCode: 403, message: 'Not allowed to perform.'}, null);

  const group = currentGroup(event);
  console.log('the group value:' + group);
  let params = {
    GroupName: group,
    UserPoolId: `${process.env.USER_POOL_ID}`,
  };

  const cognitoUsers = await provider.listUsersInGroup(params).promise();

  let users = _.map(cognitoUsers.Users, obj => {
    let user = {};
    user.status = obj.UserStatus;
    user.enable = obj.Enabled;
    _.forEach(obj.Attributes, attr => user[attr.Name] = attr.Value);
    return user;
  })

  return util.opt(null, users);
}

/**
 * the admin user only allow to delete the user belongs to his group
 *
 * @param event
 * @returns {Promise<{statusCode: number}>}
 */
module.exports.delete = async (event) => {
  if (!isAdminUser(event))
    return util.opt({statusCode: 403, message: 'Not allowed to perform.'}, null);

  const id = event.pathParameters.id;
  let params = {
    UserPoolId: `${process.env.USER_POOL_ID}`,
    Username: id
  };

  let checkMasterUser = await provider.adminGetUser(params).promise();
  if (_.find(checkMasterUser.UserAttributes, {'Name': "custom:master"})["Value"] == "true") {
    return util.opt({statusCode: 403, message: 'Can not delete Master Account.'}, null);
  }

  if (await checkPermission(event, id)) { //belongs to the same group
    await provider.adminDeleteUser(params).promise();
    return util.opt(null, "User Updated!");
  } else {
    return util.opt({statusCode: 403, message: 'Not allowed to perform.'}, null);
  }
}


/**
 * inviting a new user and move the group, this is different from the creating account
 *
 * @param event
 * @returns {Promise<void>}
 */
module.exports.inviteUser = async (event) => {
  if (!isAdminUser(event))
    return util.opt({statusCode: 403, message: 'Not allowed to perform.'}, null);

  const {email, action} = JSON.parse(event.body);
  let params = {
    UserPoolId: `${process.env.USER_POOL_ID}`,
    Username: email,
    DesiredDeliveryMediums: ["EMAIL"],
    ForceAliasCreation: false,
    UserAttributes: [
      {
        Name: 'email',
        Value: email
      },
      {
        Name: 'email_verified',
        Value: 'true'
      },
      {
        Name: 'picture',
        Value: '/img/anonymous.png'
      },
      {
        Name: 'custom:master',
        Value: "false"
      }
    ]
  };
  if (action === "resend") {
    params.MessageAction = "RESEND";
    _.remove(params.UserAttributes, {Name: 'custom:master'});
    _.remove(params.UserAttributes, {Name: 'email'});
    _.remove(params.UserAttributes, {Name: 'email_verified'});
  }

  const [error, ret] = await to(provider.adminCreateUser(params).promise());

  //if new user, move the user to group
  if (action === "invite" && ret) {
    console.log("moving the user to group");
    const groups = currentGroup(event);
    let params = {
      GroupName: groups,
      UserPoolId: `${process.env.USER_POOL_ID}`,
      Username: email
    };
    await provider.adminAddUserToGroup(params).promise();
  }
  return util.opt(error, ret);
}

/**
 *
 * @param event
 * @returns {Promise<void>}
 */
module.exports.updateUserAttributes = async (event) => {
  if (!isAdminUser(event))
    return util.opt({statusCode: 403, message: 'Not allowed to perform.'}, null);

  const {username, attributes} = JSON.parse(event.body);
  if (await checkPermission(event, username)) { //belongs to the same group
    const params = {
      UserPoolId: `${process.env.USER_POOL_ID}`,
      Username: username
    };
    params.UserAttributes = attributes;
    await provider.adminUpdateUserAttributes(params).promise();

    return util.opt(null, "User Updated!");
  }

  return util.opt({statusCode: 403, message: 'Not allowed to perform.'}, null);
}

/**
 * Batch get users by ids
 *
 * @param ids
 * @returns {Promise<[]>}
 */
module.exports.getUsersByIds = async ids => {
  let axioString = "";
  ids.forEach((id) => {
    let stringParam = JSON.stringify({UserPoolId: `${process.env.USER_POOL_ID}`, Username: id});
    axioString = axioString.concat(`provider.adminGetUser(${stringParam}).promise(),`);
  });
  let users = [];
  await Promise.allSettled (
    eval("[" + axioString + "]")
  ).then((cognitoUsers) => {
    users = _.map(cognitoUsers, obj => {
      if(obj.value) {
        let user = {};
        _.forEach(obj.value.UserAttributes, attr => {
          user[attr.Name] = attr.Value
        });
        return user;
      }
    });
  });

  return  _.compact(users);
}


/**
 * Check if the operator and the being edited user are in the same group or the user in the pending group
 *
 * @param event
 * @param username
 * @returns {Promise<boolean>}
 */
async function checkPermission(event, username) {
  let operatorGroup = currentGroup(event);
  const params = {
    UserPoolId: `${process.env.USER_POOL_ID}`,
    Username: username
  };

  let cognitoGroups = await provider.adminListGroupsForUser(params).promise();
  let userGroup = _.map(cognitoGroups.Groups, "GroupName");
  return operatorGroup === userGroup[0] || userGroup[0] === "pending";
}

/**
 * check if the current operator has admin role
 *
 * @param event
 * @returns {*}
 */
async function isAdminUser(event) {
  const params = {
    UserPoolId: `${process.env.USER_POOL_ID}`,
    Username: util.tokenClaims(event, "sub")
  }
  let user = await provider.adminGetUser(params).promise();
  return _.find(user.UserAttributes, {'Name': "custom:role"})["Value"] === "Admin";
}

/**
 *
 * @param event
 * @returns {*}
 */
function currentGroup(event) {
  //let group = event.requestContext.authorizer.jwt.claims["cognito:groups"];
  return util.tokenClaims(event, "cognito:groups");
}