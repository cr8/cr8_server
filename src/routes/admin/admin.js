module.exports = app => {

  const express = require('express');
  const dynamoose = require('../../utils/db')()
  const AWS = require('aws-sdk');

  const router = express.Router({
    mergeParams: true
  });

  app.use('/admin/api/rest/:resource', async (req, res, next) => {
    const ModelName = require('inflection').classify(req.params.resource);
    req.Model = require(`../../models/${ModelName}`)(dynamoose);
    next();
  }, router);

  router.get('/all', async (req, res) => {
    var items = await req.Model.scan().exec();
    res.send(items);
  })

  router.get('/', async (req, res) => {
    // const queryOptions = {}
    // if (req.Model.modelName === 'Property') {
    //     queryOptions.populate = 'parent'
    // }
    let companyId = req.header('companyId');
    if (req.query.ids) {
      let q = req.query.ids.split(',');
      var items = await req.Model.scan().where('id').in(q).exec();
    } else if (req.query.published) {
      var items = await req.Model.scan().filter('companyId').eq(companyId).filter('published').eq(true).exec();
    } else {
      var items = await req.Model.scan().filter('companyId').eq(companyId).exec();
    }

    res.send(items);
  })

  router.get('/:id', async (req, res) => {
    const item = await req.Model.get({
      id: req.params.id
    });
    res.send(item);
  })

  router.post('/', async (req, res) => {
    const model = await req.Model.create(req.body);
    res.send(model);
  })

  router.put('/', async (req, res) => {
    const item = await req.Model.batchPut([req.body]);
    res.send(item);
  })

  router.put('/:id', async (req, res) => {
    const item = await req.Model.batchPut([req.body]);
    res.send(item);
  })

  router.delete('/:id', async (req, res) => {
    await req.Model.delete({
      id: req.params.id
    });
    res.sendStatus(202);
  })



  const multer = require('multer');
  var multerS3 = require('multer-s3')
  var s3 = new AWS.S3();
  var upload = multer({
    storage: multerS3({
      s3: s3,
      bucket: 'cr8-storage',
      //   acl: 'public-read',
      contentType: multerS3.AUTO_CONTENT_TYPE,
      metadata: function (req, file, cb) {
        cb(null, {
          fieldName: file.fieldname
        });
      },
      key: function (req, file, cb) {
        cb(null, req.headers.companyid + "/" + req.headers.type + "/" + req.headers.id + "/" + file.originalname)
      }
    })
  })

  app.post('/admin/api/upload', upload.single('file'), async (req, res) => {
    const file = req.file;
    file.url = "https://image.weproperty.com.au/" + req.headers.companyid + "/" + req.headers.type + "/" + req.headers.id + "/" + file.originalname;
    res.send(file);
  })

  //error handler
  app.use(async (err, req, res, next) => {
    console.log(err);
    res.status(err.statusCode || 500).send({
      message: err.message
    })
  })


}