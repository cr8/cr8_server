module.exports = dynamoose => {
  const shortid = require('shortid');
  const schema = new dynamoose.Schema({
    id: {
      type: String,
      default: shortid.generate,
      hashKey: true
    },
    companyId: {
      type: String,
      index: {
        global: true,
        project: true, // ProjectionType: ALL
        throughput: 1 // read and write are both 5
      }
    },
    agents:{ type: Array },
  })

  return dynamoose.model('Collection', schema);
}