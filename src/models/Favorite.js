module.exports = dynamoose => {
  const schema = new dynamoose.Schema({
    userId: {
      type: String,
      hashKey: true
    },
    projectId: {
      type: String,
      rangeKey: true,
    }
    },
    {
      timestamps: true
    })

  return dynamoose.model('Favorite', schema);
}