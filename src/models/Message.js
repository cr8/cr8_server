module.exports = dynamoose => {
  const shortid = require('shortid');
  const MessageSchema = new dynamoose.Schema({
      id: {
        type: String,
        default: shortid.generate,
        hashKey: true
      },
      email: {
        type: String,
        index: {
          global: true,
          project: true, // ProjectionType: ALL
          throughput: 1 // read and write are both 5
        }
      },
      // createdAt: {
      //   type: Number,
      //   rangeKey: true,
      //   required: true,
      //   default: Date.now,
      // },
      subject: {
        type: String,
      },
      body: {
        type: String,
      },
      viewed: {
        type: Boolean,
        default: false
      },
    },
    {
      timestamps: true
    }
  )

  return dynamoose.model('Message', MessageSchema);
}