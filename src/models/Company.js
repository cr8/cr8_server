module.exports = dynamoose => {
    const shortid = require('shortid');
    const schema = new dynamoose.Schema({
        id: { type: String, hashKey: true}, //the companyId
        name: { type: String },
        logo: {type: String},
        flag: {type: String,default: shortid.generate},
        website: {type: String},
        address: {type: String},
        lat: {type: Number},
        lng: {type: Number},
        phone: {type: String},
        email: {type:String},
        desc: {type: String},
    })

    return dynamoose.model('Company', schema);
}