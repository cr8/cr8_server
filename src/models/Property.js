module.exports = dynamoose => {
  const shortid = require('shortid');
  const schema = new dynamoose.Schema({
      companyId: {
        type: String,
        hashKey: true
      },
      id: {
        type: String,
        rangeKey: true,
        default: shortid.generate
      },
      parentId: {
        type: String,
        index: {
          name: "parentIndex",
          throughput: 1
        }
      },
      originId: {
        type: String,
        index: {
          name: "originIdIndex",
          throughput: 1 // read and write are both 5
        }
      },
      published: {type: Boolean, default: false},
      projectName: {type: String},
      projectStatus:{type:String},
      location: {type: String},
      address: {type: String},
      minRoom: {type: String},
      maxRoom: {type: String},
      minToilet: {type: String},
      maxToilet: {type: String},
      garage: {type: String},
      availProperties: {type: Number},
      totalProperties: {type: String},
      minPrice: {type: String},
      maxPrice: {type: String},
      estCompletion: {type: String},
      desc: {type: String},
      cn_desc: {type: String},
      status: {type: String},
      category: {type: String},
      lastUpdate: {type: Date},
      images: [{
        url: {type: String}
      }],
      videos: [{
        url: {type: String},
        uid: {type: String},
        video: {type: String}
      }],
      attachments: [{
        url: {type: String},
        name: {type: String},
        desc: {type: String},
        size: {type: String},
        fileType: {type: String},
        lastUpdate: {type: String}
      }],
      priceList: [{
        id: {type: String},
        storey:{type:String},
        room: {type: String},
        toilet: {type: String},
        garage: {type: String},
        internalArea: {type: String},
        landArea: {type: String},
        price: {type: String},
        status: {type: Boolean},
        floorPlan:{type:Array}
      }],
      lat: {type: Number},
      lng: {type: Number},
      suburb: {type: String},
      // features: [{name:{type:String}}],
      schools: {type: Array},
      transport: {type: Array},
      sales: {type: Array},
      agents: {type: Array},
      commission: {type: String},
      gst:{type: Boolean},
      public:{type: Boolean, default: false},
      fromAgent: {type: String},
      fromAgentId: {type: String},
      fromAgentCOMSN:{type: String},
    },
    {timestamps: true})

  return dynamoose.model('Property', schema);
}